# Organizer
Приложение органайзер призвано помочь в распределении задач и дедлайнов.
Данное приложение выполнено в рамках курсового проекта по дисциплине ТиМП

## Prerequisites
Перед тем как запустить приложение убедитесь, что у вас установлены следующие инструменты:
* Docker
* Docker-compose

## Installing organizer app
Для запуска приложения следуйте следующим инструкциям:<br>
Склонируйте этот репозиторий<br>
* Linux, Windows:
```shell script
git clone https://gitlab.com/Mishinator/kursa4.git
``` 
После вам необходимо собрать docker-контейнеры и запустить их:<br>
* Linux, Windows:
```shell
docker-compose up -d --build
```

После вам необходимо установить зависимости с помощью менеджера пакетов Composer:<br>
* Linux:
```shell
docker-compose run --rm composer install
```

* Windows:
```shell
docker-compose exec php /bin/ash
```
```shell
php composer install
```
После необходимо скомпилировать css стили:
* Windows, Linux:
```shell
docker-compose run --rm npm install
```

```shell
docker-compose run --rm npm run dev
```

После вам необходимо запустить миграции БД:
* Linux:
```shell
docker-compose run --rm artisan migrate
```
* Windows:
```shell
docker-compose exec php /bin/ash
```
```shell
php artisan migrate
```

## Using organizer app
Получить доступ к приложению можно через браузер по адресу ```127.0.0.1:8088```


## Running tests
Для запуска тестов необходимо выполнить следующую команду:<br>
* Linux:
```shell
docker-compose run --rm artisan test
```

* Windows:
```shell
docker-compose exec php /bin/ash
```
```shell
php artisan test
```

## Code style
Кодовая база данного приложения придерживается [PSR-12](https://www.php-fig.org/psr/psr-12/)

## Screens
* Главная страница
![Main page](images/start_page.png)  


* Страница регистрации
![Registration](images/register_page.png)
  

* Главная страница пользователя
![Dashboard](images/dashboard_page.png)
  

* Страница с досками
![Boards](images/boards.png)
  

* Страница с напоминаниями
![Reminders](images/reminder.png)
  

* Календарь
![Calendar](images/calendar.png)


