<?php

use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\CalendarController;
use App\Http\Controllers\ReminderController;
use App\Http\Controllers\TaskController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BoardController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');


// TODOList routes
Route::name('boards.')->prefix('boards')->group(function () {
    // Boards routes
    Route::get('/', [BoardController::class, 'index'])->name('index');
    Route::get('/create', [BoardController::class, 'create'])->name('create');
    Route::post('/', [BoardController::class, 'store'])->name('store');
    Route::get('/{id}/edit', [BoardController::class, 'edit'])->name('edit');
    Route::put('/{id}', [BoardController::class, 'update'])->name('update');
    Route::delete('/{id}', [BoardController::class, 'destroy'])->name('destroy');

    // Tasks routes
    Route::name('tasks.')->prefix('{board_id}/tasks')->group(function () {
        Route::get('/create', [TaskController::class, 'create'])->name('create');
        Route::post('/', [TaskController::class, 'store'])->name('store');
        Route::get('/{id}/edit', [TaskController::class, 'edit'])->name('edit');
        Route::put('/{id}', [TaskController::class, 'update'])->name('update');
        Route::delete('/{id}', [TaskController::class, 'destroy'])->name('destroy');
    });
});


// Reminder routes
Route::name('reminders.')->prefix('reminders')->group(function () {
    Route::get('/', [ReminderController::class, 'index'])->name('index');
});


// Calendar routes
Route::name('calendar.')->prefix('calendar')->group(function () {
   Route::get('/', [CalendarController::class, 'index'])->name('index');
});


// Profile routes
Route::name('profile.')->prefix('profile')->middleware('auth')->group(function () {
    Route::get('/', [RegisteredUserController::class, 'index'])->name('index');
    Route::get('/editNameAndEmail', [RegisteredUserController::class, 'editNameAndEmail'])->name('editNameAndEmail');
    Route::get('/editPassword', [RegisteredUserController::class, 'editPassword'])->name('editPassword');
    Route::put('/updatePassword', [RegisteredUserController::class, 'updatePassword'])->name('updatePassword');
    Route::put('/updateEmailAndName', [RegisteredUserController::class, 'updateNameAndEmail'])->name('updateNameAndEmail');
});

require __DIR__.'/auth.php';
