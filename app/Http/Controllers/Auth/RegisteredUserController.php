<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RegisteredUserController extends Controller
{

    public function index()
    {
        return view('profile.index', ['user' => Auth::user()]);
    }

    public function create()
    {
        return view('auth.register');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|confirmed|min:8',
        ]);

        Auth::login($user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]));

        event(new Registered($user));

        return redirect(RouteServiceProvider::HOME);
    }

    public function editNameAndEmail()
    {
        return view('profile.edit_name', ['user' => Auth::user()]);
    }

    public function editPassword()
    {
        return view('profile.edit_password');
    }

    public function updateNameAndEmail(Request $request)
    {
        $request->validate([
            'name' => 'string|max:255',
            'email' => 'string|max:255',
                               ]);

        $user = Auth::user();

        if ($request->has('name')) {
           $user->name = $request->name;
        }

        if ($request->has('email')) {
            $user->email = $request->email;
        }


        $user->save();

        return view('profile.index', ['user' => Auth::user()]);
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
                               'name' => 'string|max:255',
                               'email' => 'string|max:255',
                               'password' => 'required|string|confirmed|min:8',
                           ]);

        $user = Auth::user();

        $user->password = Hash::make($request->password);

        $user->save();

        return view('profile.index', ['user' => Auth::user()]);
    }
}
