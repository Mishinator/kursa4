<?php

namespace App\Http\Controllers;

use App\Models\Board;
use App\Models\Task;
use Carbon\Carbon;
use Illuminate\Http\Request;


class TaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create($board_id)
    {
        $board = Board::find($board_id);
        return view('boards.tasks.create', ['board' => $board]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|max:255',
            'description' => 'required|max:255',
            'deadline' => 'required',
            'board_id' => 'required'
                           ]);

        $task = new Task();
        $task->title = $request->input('title');
        $task->description = $request->input('description');
        $task->deadline = $request->input('deadline');
        $task->board_id = $request->input('board_id');
        $task->save();

        return redirect()->route('boards.index');
    }

    public function edit($board_id, $id)
    {
        $task = Task::find($id);
        return view('boards.tasks.edit', ['task' => $task]);
    }

    public function update(Request $request, $board_id, $id)
    {
        $request->validate([
            'title' => 'required|max:255',
            'description' => 'required|max:255',
            'deadline' => 'required',
                           ]);

        $task = Task::find($id);
        $task->title = $request->input('title');
        $task->description = $request->input('description');
        $task->status = $request->input('status');
        $task->deadline = $request->input('deadline');
        if ($request->has('notify')) {
            $task->notify = $request->input('notify');
        }

        if ($request->input('status') === 'completed') {
            $task->completion_date = Carbon::now();
        }
        $task->save();

        return redirect()->route('boards.index');
    }

    public function destroy($board_id, $id)
    {
        $task = Task::find($id);
        $task->delete();
        return redirect()->route('boards.index');
    }
}
