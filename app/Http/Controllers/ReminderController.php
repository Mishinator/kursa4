<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class ReminderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $boards = Auth::user()->boards;
        $tasks = collect([]);
        foreach ($boards as $board) {
            foreach ($board->tasks as $task) {
                if ($task->isSetReminder() && !$task->isCompleted()) {
                    $tasks->push($task);
                }
            }
        }

        if ($request->filled('sort')) {
            $tasks = $tasks->sortBy('deadline');
        }

        return view('reminders.index', ['tasks' => $tasks]);
    }
}
