<?php

namespace App\Http\Controllers;

use App\Models\Board;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BoardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();
        return view('boards.index', ['boards' => $user->boards]);
    }

    public function create()
    {
        return view('boards.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
                           ]);

        $board = new Board();
        $board->name = $request->input('name');
        $board->color = $request->input('color');
        $board->user_id = Auth::id();
        $board->save();

        return redirect()->route('boards.index');
    }

    public function edit($id)
    {
        $board = Board::find($id);
        return view('boards.edit', ['board' => $board]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required'
                           ]);

        $board = Board::find($id);
        $board->name = $request->input('name');
        $board->color = $request->input('color');
        $board->save();

        return redirect()->route('boards.index');
    }

    public function destroy($id)
    {
        $board = Board::find($id);
        $tasks = $board->tasks;
        foreach ($tasks as $task) {
            $task->delete();
        }
        $board->delete();

        return redirect()->route('boards.index');
    }
}
