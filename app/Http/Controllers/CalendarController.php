<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\Auth;


class CalendarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $boards = Auth::user()->boards;
        $tasks = collect([]);
        foreach ($boards as $board) {
            foreach ($board->tasks as $task) {
                $tasks->push($task);
            }
        }

        $days_info = collect([]);

        if ($tasks->isEmpty()) {
            return view('calendar.index', ['days_info' => $days_info]);
        }

        $tasks = $tasks->sortBy('deadline');
        $start_day = Carbon::create($tasks->first()->deadline)->startOfWeek();
        $end_day = $tasks->last()->deadline;
        $period = CarbonPeriod::create($start_day, $end_day);

        foreach ($period as $day) {
            $days_info->push(['day' => $day, 'tasks' => collect([])]);
        }

        $i = 0;
        foreach ($tasks as $task) {
            while (!$days_info[$i]['day']->isSameDay(Carbon::create($task->deadline))) {
                ++$i;
            }
            $days_info[$i]['tasks']->push($task);
        }

        return view('calendar.index', ['days_info' => $days_info]);
    }
}
