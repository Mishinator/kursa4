<x-app-layout>
    <x-slot name="header">
        Календарь
    </x-slot>

    <div class="mt-5 grid gap-4 grid-cols-7">
        <!--Заголовик дней недели-->
        <div class=" p-5 rounded-xl shadow-md bg-yellow-500">Понедельник</div>
        <div class=" p-5 rounded-xl shadow-md bg-yellow-500">Вторник</div>
        <div class=" p-5 rounded-xl shadow-md bg-yellow-500">Среда</div>
        <div class=" p-5 rounded-xl shadow-md bg-yellow-500">Четверг</div>
        <div class=" p-5 rounded-xl shadow-md bg-yellow-500">Пятница</div>
        <div class=" p-5 rounded-xl shadow-md bg-yellow-500">Суббота</div>
        <div class=" p-5 rounded-xl shadow-md bg-yellow-500">Воскресенье</div>

        <!--Дни-->
        @foreach($days_info as $day_info)
            @if($day_info['tasks']->isEmpty())

                <!--День и месяц-->
                <div class=" p-5 rounded-xl shadow-md bg-gray-400">
                    {{ $day_info['day']->format('F-d') }}
                </div>

            @else
                <div class=" p-5 rounded-xl shadow-md bg-yellow-500">
                    <!--День и месяц-->
                    {{ $day_info['day']->format('F-d') }}

                    <div class="space-y-5">

                        <!--Задачи данного дня-->
                    @foreach($day_info['tasks'] as $task)
                        <div class="p-6 max-w-sm mx-auto bg-white rounded-xl shadow-md flex-column items-center space-y-4">

                            <!--Статус задачи-->
                            @if(!$task->isCompleted())
                                <p class="text-xs text-gray-700">{{ $task->status }}</p>
                            @else
                                <p class="text-lg text-green-700">{{ $task->status }}</p>
                            @endif

                            <!--Заголовок задачи-->
                            <div class="text-xl font-medium text-black">{{ $task->title }}</div>

                            <!--Описание задачи-->
                            <p class="text-gray-500">{{ $task->description }}</p>

                            <!--Deadline задачи-->
                            <div>
                                <p class="text-red-500">Deadline: </p>
                                <p><time>{{ $task->deadline }}</time></p>
                            </div>

                            <!--Дата исполнения задачи-->
                            @if($task->isCompleted())
                                <p class="text-green-500">Completed at: </p>
                                <p><time>{{ $task->completion_date }}</time></p>
                            @endif

                        </div>
                    @endforeach

                    </div>
                </div>
            @endif
        @endforeach
    </div>
</x-app-layout>
