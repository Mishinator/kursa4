<x-app-layout>
    <x-slot name="header">
        Изменение профиля
    </x-slot>
    <div class="p-10">
        <x-auth-validation-errors class="mb-4" :errors="$errors" />
        <form method="POST" action="{{ route('profile.updatePassword') }}">
        @method('PUT')
        @csrf

            <!-- Password -->
            <div class="mt-4">
                <x-label for="password" :value="__('New Password')" />

                <x-input id="password" class="block mt-1 w-full"
                         type="password"
                         name="password"
                />
            </div>

            <!-- Confirm Password -->
            <div class="mt-4">
                <x-label for="password_confirmation" :value="__('Confirm New Password')" />

                <x-input id="password_confirmation" class="block mt-1 w-full"
                         type="password"
                         name="password_confirmation" />
            </div>

            <div class="flex items-center justify-end mt-4">

                <x-button class="ml-4">
                    {{ __('Изменить') }}
                </x-button>
            </div>
        </form>
    </div>
</x-app-layout>
