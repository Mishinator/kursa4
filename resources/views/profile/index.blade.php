<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Профиль
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="focus:bg-blue p-6 text-center bg-white border-b border-gray-200">
                    Name: {{ $user->name }}
                </div>

                <div class="focus:bg-blue p-6 text-center bg-white border-b border-gray-200">
                    email: {{ $user->email }}
                </div>
                <div>
                    <form method="GET" action="{{ route('profile.editPassword', [$user->id]) }}">
                        @csrf
                        <button class="bg-blue-500 hover:bg-red-700 text-white font-bold py-2 px-4 border rounded">
                            Изменить пароль
                        </button>
                    </form>
                </div>

                <div>
                    <form method="GET" action="{{ route('profile.editNameAndEmail', [$user->id]) }}">
                        @csrf
                        <button class="bg-blue-500 hover:bg-red-700 text-white font-bold py-2 px-4 border rounded">
                            Изменить почту или имя
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
