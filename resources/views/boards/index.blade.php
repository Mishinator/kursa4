<x-app-layout>
    <x-slot name="header">
        Мои доски
    </x-slot>

    <!--Кнопка создания доски-->
    <div class="m-5">
        <form method="GET" action="{{ route('boards.create') }}">
            @csrf
            <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded">
                Создать доску
            </button>
        </form>
    </div>


    <div class="m-5">
        @foreach($boards as $board)

            <!--Блок доски-->
            <div id="board_{{ $board->id }}" class="inline-flex mt-10 mr-10 p-10 max-w-sm mx-auto rounded-xl shadow-md flex-1 items-center space-y-4">
                <style>
                    #board_{{$board->id}} {
                        background-color: {{ $board->color }};
                    }
                </style>
                <div class="space-y-10">

                    <!--Имя доски-->
                    <div class="mb-10 font-bold text-3xl">
                        {{ $board->name }}
                    </div>

                    <!--Задачи доски-->
                    <div class="space-y-10">
                        @foreach($board->tasks as $task)
                            <div class="p-6 max-w-sm mx-auto bg-white rounded-xl shadow-md flex-column items-center space-y-4">

                                <!--Статус задачи-->
                                @if(!$task->isCompleted())
                                    <p class="text-xs text-gray-700">{{ $task->status }}</p>
                                @else
                                    <p class="text-lg text-green-700">{{ $task->status }}</p>
                                @endif

                                <!--Заголовок задачи-->
                                <div class="text-xl font-medium text-black">{{ $task->title }}</div>

                                <!--Описание задачи-->
                                <p class="text-gray-500">{{ $task->description }}</p>

                                <!--Deadline задачи-->
                                <div>
                                    <p class="text-red-500">Deadline: </p>
                                    <p><time>{{ $task->deadline }}</time></p>
                                </div>

                                <!--Дата исполнения задачи-->
                                    @if($task->isCompleted())
                                        <p class="text-green-500">Completed at: </p>
                                        <p><time>{{ $task->completion_date }}</time></p>
                                    @endif

                                <!--Кнопка удаления задачи-->
                                <div>
                                    <form method="POST" action="{{ route('boards.tasks.destroy', [$task->board_id, $task->id]) }}">
                                        @csrf
                                        @method('DELETE')
                                        <button class="bg-blue-500 hover:bg-red-700 text-white font-bold py-2 px-4 border rounded">
                                            Удалить
                                        </button>
                                    </form>
                                </div>

                                <!--Кнопка изменения задачи-->
                                <div>
                                    <form method="GET" action="{{ route('boards.tasks.edit', [$task->board_id, $task->id]) }}">
                                        @csrf
                                        <button class="bg-blue-500 hover:bg-red-700 text-white font-bold py-2 px-4 border rounded">
                                            Изменить
                                        </button>
                                    </form>
                                </div>

                                @if(!$task->isCompleted())

                                        <!--Кнопка напоминания задачи-->
                                        <div>
                                            <form method="POST" action="{{ route('boards.tasks.update', [$task->board_id, $task->id]) }}">
                                                @csrf
                                                @method('PUT')
                                                <input type="hidden" name="title" value="{{ $task->title }}">
                                                <input type="hidden" name="description" value="{{ $task->description }}">
                                                <input type="hidden" name="deadline" value="{{ $task->deadline }}">
                                                <input type="hidden" name="status" value="incomplete">
                                                @if($task->isSetReminder())
                                                    <input type="hidden" name="notify" value="false">
                                                    <button class="bg-blue-500 hover:bg-red-700 text-white font-bold py-2 px-4 border rounded">
                                                        Убрать напоминание
                                                    </button>
                                                @else
                                                    <input type="hidden" name="notify" value="true">
                                                    <button class="bg-blue-500 hover:bg-red-700 text-white font-bold py-2 px-4 border rounded">
                                                        Напомнить
                                                    </button>
                                                @endif
                                            </form>
                                        </div>

                                    @endif
                            </div>
                        @endforeach
                    </div>

                    <!--Кнопки управления-->
                    <div class="space-y-4">

                        <!--Кнопка создания задачи-->
                        <div>
                            <form method="GET" action="{{ route('boards.tasks.create', $board->id) }}">
                                @csrf
                                <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded">
                                    Создать задачу
                                </button>
                            </form>
                        </div>

                        <!--Кнопка изменения доски-->
                        <div>
                            <form method="GET" action="{{ route('boards.edit', $board->id) }}">
                                @csrf
                                <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded">
                                    Изменить доску
                                </button>
                            </form>
                        </div>

                        <!--Кнопка удаления доски-->
                        <div>
                            <form method="POST" action="{{ route('boards.destroy', $board->id) }}">
                                @csrf
                                @method('DELETE')
                                <button class="bg-blue-500 hover:bg-red-700 text-white font-bold py-2 px-4 border border-blue-700 rounded">
                                    Удалить доску
                                </button>
                            </form>
                        </div>

                    </div>

                </div>
            </div>
        @endforeach
    </div>

</x-app-layout>
