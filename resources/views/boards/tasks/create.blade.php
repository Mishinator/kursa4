<x-app-layout>
    <x-slot name="header">
        Создание задачи для доски {{ $board->name }}
    </x-slot>


    <div class="m-10 p-10 max-w-sm mx-auto bg-white rounded-xl shadow-md flex-column space-y-4 items-center space-x-4">

        <!--Ошибки ввода-->
        @if ($errors->any())
            <div class="alert">
                <ul class="list-disc">
                    @foreach ($errors->all() as $error)
                        <li class="text-red-700">{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div>
            <form action="{{ route('boards.tasks.store', $board->id) }}" method="POST" class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                @csrf
                <div class="flex-shrink-0">

                    <div>
                        <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="task">Загловок задачи</label>
                        <input class="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" id="inline-full-name" type="text" name="title">
                    </div>

                    <div class="mt-10">
                        <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="task">Описание задачи</label>
                        <p><textarea class="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" id="inline-full-name" rows="10" cols="45" name="description"></textarea></p>
                    </div>

                    <div class="mt-10">
                        <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="task">Deadline</label>
                        <input class="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" type="date" name="deadline">
                    </div>

                    <div class="mt-10">
                        <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="task">Status</label>
                        <select class="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" name="status" size="1">
                            <option value="incomplete">Не готова</option>
                            <option value="completed">Готова</option>
                        </select>
                    </div>

                    <input type="hidden" name="board_id" value="{{ $board->id }}">

                    <button class="mt-10 py-2 px-4 font-semibold rounded-lg shadow-md text-white bg-green-500 hover:bg-green-700" type="submit">
                        Создать
                    </button>

                </div>
            </form>
        </div>

    </div>

</x-app-layout>
