<x-app-layout>
    <x-slot name="header">
        Изменение доски
    </x-slot>

    <div class="m-10 p-10 max-w-sm mx-auto bg-white rounded-xl shadow-md flex-column items-center space-y-4 space-x-4">

        <!--Ошибки ввода-->
        <div>
            @if ($errors->any())
                <div class="alert">
                    <ul class="list-disc">
                        @foreach ($errors->all() as $error)
                            <li class="text-red-700">{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>

        <div>
            <form action="{{ route('boards.update', $board->id) }}" method="POST" class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                @method('PUT')
                @csrf
                <div class="flex-shrink-0">

                    <div>
                        <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="board">Навзвание доски</label>
                        <input class="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" type="text" name="name" id="task-name" value="{{ $board->name }}">
                    </div>

                    <div class="mt-10">
                        <label class="p-5" for="board">Цвет</label>
                        <select class="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" name="color" size="1">
                            <option value="#E6E6FA">Белый</option>
                            <option value="#ADFF2F">Зелёный</option>
                            <option value="#00FFFF">Синий</option>
                        </select>
                    </div>

                    <button class="mt-10 py-2 px-4 font-semibold rounded-lg shadow-md text-white bg-green-500 hover:bg-green-700" type="submit">
                        Изменить
                    </button>

                </div>
            </form>
        </div>

    </div>

</x-app-layout>
