<x-app-layout>
    <x-slot name="header">
        Напоминания
    </x-slot>

    <!--Кнопка сортировки-->
    <div class="m-5 align-items-center">
        <form method="GET" action="{{ route('reminders.index') }}">
            @csrf
            <input type="hidden" name="sort" value="sort">
            <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded">
                Отсортировать
            </button>
        </form>
    </div>

    <div class="space-y-5 m-5 flex-column">
        @foreach($tasks as $task)
            <div class="p-6 max-w-sm mx-auto bg-white rounded-xl shadow-md flex-column items-center space-y-4">

                <p class="text-xs text-gray-700">{{ $task->status }}</p>

                <div class="text-xl font-medium text-black">{{ $task->title }}</div>

                <p class="text-gray-500">{{ $task->description }}</p>

                <div>
                    <p class="text-red-500">Deadline: </p>
                    <p><time>{{ $task->deadline }}</time></p>
                </div>
            </div>
        @endforeach
    </div>

</x-app-layout>
