<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Приложение органайзер') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">

                <a href="{{ route('profile.index') }}">
                    <div class="p-6 text-center bg-white border-b border-gray-200">
                        Профиль
                    </div>
                </a>

                <a href="{{ route('boards.index') }}">
                    <div class="focus:bg-blue p-6 text-center bg-white border-b border-gray-200">
                        Доски
                    </div>
                </a>
                <a href="{{ route('reminders.index') }}">
                    <div class="p-6 text-center bg-white border-b border-gray-200">
                        Напоминания
                    </div>
                </a>
                <a href="{{ route('calendar.index') }}">
                    <div class="p-6 text-center bg-white border-b border-gray-200">
                        Календарь
                    </div>
                </a>
                <a>
                    <div class="p-6 text-center bg-white border-b border-gray-200">
                        Настройки
                    </div>
                </a>
            </div>
        </div>
    </div>
</x-app-layout>
