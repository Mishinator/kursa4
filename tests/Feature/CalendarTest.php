<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CalendarTest extends TestCase
{
    use RefreshDatabase;

    public function testCalendarScreenCanBeRendered()
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)->get(route('calendar.index'));
        $response->assertStatus(200);
    }

}
