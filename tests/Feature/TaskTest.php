<?php

namespace Tests\Feature;

use App\Models\Board;
use App\Models\Task;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TaskTest extends TestCase
{
    use RefreshDatabase;

    public function testTaskCreateScreenCanBeRendered()
    {
        $user = User::factory()->has(Board::factory()->count(1))->create();
        $response = $this->actingAs($user)->get(route('boards.tasks.create', $user->boards->first()->id));
        $response->assertStatus(200);
    }
}
