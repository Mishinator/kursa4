<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ReminderTest extends TestCase
{
    use RefreshDatabase;

    public function testReminderPageCanBeRendered()
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)->get(route('reminders.index'));
        $response->assertStatus(200);
    }
}
