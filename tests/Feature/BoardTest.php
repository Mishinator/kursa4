<?php

namespace Tests\Feature;

use App\Models\Board;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BoardTest extends TestCase
{
    use RefreshDatabase;

    public function testBoardsIndexPageCanBeRendered()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->get(route('boards.index'));
        $response->assertStatus(200);
    }

    public function testBoardCreatePageCanBeRendered()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->get('/boards/create');
        $response->assertStatus(200);
    }

    public function testCanCreateBoard()
    {
        $user = User::factory()->create();

        $board_name = 'cool_name';
        $board_color = 'green';

        $this->assertDatabaseMissing('boards', [
            'name' => $board_name,
            'color' => $board_color,
            'user_id' => $user->id,
        ]);

        $response = $this->actingAs($user)->post(route('boards.store', [
            'name' => $board_name,
            'color' => $board_color
        ]));

        $response->assertStatus(302);
        $this->assertDatabaseHas('boards', [
            'name' => $board_name,
            'color' => $board_color,
            'user_id' => $user->id,
        ]);
    }

    public function testBoardEditPageCanBeRendered()
    {
        $user = User::factory()->has(Board::factory()->count(1))->create();

        $this->assertDatabaseHas('boards', [
            'user_id' => $user->id,
            'name' => $user->boards->first()->name,
            'color' => $user->boards->first()->color,
        ]);

        $response = $this->actingAs($user)->get(route('boards.edit', $user->boards->first()->id));
        $response->assertStatus(200);
    }

    public function testCanUpdateBoard()
    {
        $user = User::factory()->has(Board::factory()->count(1))->create();

        $this->assertDatabaseHas('boards', [
            'user_id' => $user->id,
            'name' => $user->boards->first()->name,
            'color' => $user->boards->first()->color,
        ]);

        $board_new_name = 'new_cool_name';
        $board_new_color = 'new_cool_color';

        $response = $this->actingAs($user)->put(route('boards.update', [
            'id' => $user->boards->first()->id,
            'name' => $board_new_name,
            'color' => $board_new_color,
        ]));
        $response->assertStatus(302);

        $this->assertDatabaseHas('boards', [
            'user_id' => $user->id,
            'name' => $board_new_name,
            'color' => $board_new_color,
        ]);
    }

    public function testCanDeleteBoard()
    {
        $user = User::factory()->has(Board::factory()->count(1))->create();

        $this->assertDatabaseHas('boards', [
            'user_id' => $user->id,
            'name' => $user->boards->first()->name,
            'color' => $user->boards->first()->color,
        ]);

        $response = $this->actingAs($user)->delete(route('boards.update', $user->boards->first()->id));
        $response->assertStatus(302);

        $this->assertDatabaseMissing('boards', [
            'user_id' => $user->id,
            'name' => $user->boards->first()->id,
            'color' => $user->boards->first()->id,
        ]);
    }
}
